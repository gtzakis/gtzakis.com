# [gtzakis.com](https://gtzakis.com/)

This is the repo where I store my personal site. It is just a [Hugo](https://gohugo.io/) static site and I use [hello-friend-ng](https://github.com/rhazdon/hugo-theme-hello-friend-ng) theme which is pretty cool.

As for the infra, just a Digital Ocean droplet and some Nginx containers do the trick. :)
