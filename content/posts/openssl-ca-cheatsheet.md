---
title: "OpenSSL CA CheatSheet"
date: 2022-03-11T17:20:13+02:00
type: ["posts","post"]
tags: [ "security", "certificates", "pki", "openssl", "linux" ]
categories: [ "Sysadmin", "Security", "Linux"]
---

This is a quick cheatsheet style post to create a self-signed Certificate Authority using OpenSSL tool for your lab environment.
This is still WIP, i want to enchance it with an Intermediate CA to follow the security best practises!

First we need to create the private key and the certificate of our Root CA.

### Generate the Root private key `ca-key.pem`

```bash
openssl genrsa -aes256 -out ca-key.pem 4096
```

### Create the Root CA certificate `ca-cert.pem`

```bash
openssl req -key ca-key.pem -new -x509 -days 7300 -sha256 -out ca-cert.pem
```

#### Validate

```bash
openssl x509 -in ca-cert.pem -text
```

Then we need to generate the private key, Certificate Signing Request (CSR) and at last the server certificate.

### Generate a server RSA key `server-key.pem`

```bash
openssl genrsa -out server-key.pem 2048
```

### Create a Certificate Signing Request (CSR) `server-csr.pem`

```bash
openssl req -new -sha256 -subj "/CN=www.makis.com" -key server-key.pem -out server-csr.pem
```

#### Create a extfile with all the Subject Alternative Names, eg. for the IP addresses

```bash
echo "subjectAltName=DNS:www.makis.com,IP:10.25.10.10" >> extfile.cnf
# optional
echo extendedKeyUsage = serverAuth >> extfile.cnf
```

### Create-sign the Server certificate `server-cert.pem`

```bash
openssl x509 -req -sha256 -days 365 -in server-csr.pem -CA ca-cert.pem -CAkey ca-key.pem -out server-cert.pem -extfile extfile.cnf -CAcreateserial
```

#### Validate & Verify

```bash
openssl x509 -in ca-cert.pem -text
openssl verify -CAfile ca-cert.pem -verbose server-cert.pem
```

Now if you import the server certificate it should be working.

**!Remember, you have to import the Root CA cert in your private certificate store in order to trust it!**
