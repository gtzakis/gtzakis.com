---
title: "About"
date: 2019-03-30T14:52:34+02:00
aliases: ["about-me"]
---

Hello there,
my name is Gerasimos Tzakis but everybody calls me *Makis*.

I have studied Computer Science, and i currently live and work in Athens, Greece.
I find huge interest in Linux based systems and Open source technologies and right now, my main focuses are on Cyber Security, Systems/Network Engineering and automation.

I believe that true peace of mind is achieved only by long talks and hard workout sessions.
On my spare time you'll probably see me drinking beer with friends, working out, developing or running labs on something new.

**Cheers!**
