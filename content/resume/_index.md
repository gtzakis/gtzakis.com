---
title: "Resume"
date: 2019-03-30T14:52:57+02:00
aliases: ["my-resume"]
---
## Gerasimos Tzakis

### Network Automation & Security Engineer

> [LinkedIn](https://www.linkedin.com/in/gtzakis/)
> [gertzakis@gmail.com](mailto:gertzakis@gmail.com)
> [Github](https://github.com/gertzakis)

---

### Profile {#profile}

Highly motivated and capable Systems Engineer, trying to make things work smoothly with high standards of Information Security in mind. Currently trying to balance between Network Automation and SRE practices. Always looking for the optimal way of designing and implementing solutions. What I enjoy in the field is challenging myself with a problem that needs thinking outside the box and learning the latest technologies in the industry.

---

### Skills {#skills}

* **Linux**
  : Advanced knowledge in administrating POSIX based operating systems.

* **Networking**
  : Knowledge of networking protocols, and skilled in advanced troubleshooting of large enterprise topologies.

* **Automation**
  : Developing solutions for automating network operations.

---

### Technical {#technical}

1. **Firewalls**: Checkpoint, FortiGate
1. **Load Balancers**: Citrix, F5
1. **Networking**: Routing, switching, FRR FTW
1. **Linux**: bash, regex, everything in terminal
1. **Containers**: Docker, Kubernetes
1. **Cloud**: Azure, AWS
1. **Programming**: Python, Ansible, C# when I was young
1. **Network Automation**: Nautobot, Nornir, NAPALM
1. **DevOps**: Github Actions, Gitlab CI

---

### Experience {#experience}

[**Network To Code LLC**](https://www.networktocode.com/)
: **Senior Network Automation Engineer**
  ***Feb 2025 - Present***
: **Network Automation Engineer**
  ***Aug 2022 - Jan 2025***
: I have to write something over here... Someday... But there are a lot. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
<p>

[**UniSystems SMSA**](https://unisystems.com/)
: **Professional Services Network Engineer**
  ***Jan 2021 - Jul 2022***
: Working for one of the biggest system integrators in Greece, I was exposed to multiple different technologies adjacent to networking such as Load Balancing (Citrix ADC), Firewalls (Check Point, FortiGate), Cloud deployments (Azure). In most of the projects I was involved, I applied Automation/DevOps/IaC principles using tools like Ansible, Python, and Terraform.
<p>

[**Odyssey Consultants Ltd**](https://www.odysseycs.com/)
: **Cyber Security Engineer**
  ***Jun 2018 - Dec 2020***
: My main responsibilities, as a Cyber Security Engineer at Odyssey, were integrating and administrating different security systems (Check Point NGFW, Imperva WAF, F5 BIG-IP etc.) and networks. Troubleshooting on complex networks, both physical and virtual, was sure an interesting challenge. On my last days, a great deal of my time was spent on configuring and integrating security systems on Cloud (mainly Azure).
<p>

[**Alexander Moore SA**](https://www.alexandermoore.com/main/)
: **Software Developer**
  ***Jun 2017 - Dec 2017***
: Alexander Moore is one of the leaders of SAP Partners in Greece. When i was working there i was in development and in the maintenance of projects for SAP B1 in Vb/C# .NET with SAP framework, SQL scripts-queries on MSSQL & SAP Hana and also maintaining multiple DB servers with MSSQL and SAP Hana. Furthermore working in projects with a lot of business logic(ERP integrations), i became familiar with the industry and the business side of technology.
<p>

**Prologic SA**
: **System Administrator**
  ***Feb 2016 - May 2016***
: In my brief working period at Prologic, i was integrating and troubleshooting systems for the various customers of the company. I also became more aware of the Linux systems, especially SUSE.
<p>

[**National Bank of Greece Leasing SA**](http://www.ethnolease.gr/)
: ***Junior System Administrator***
  ***Apr 2015 - Dec 2015***
: ***IT Intern***
  ***Oct 2014 - Apr 2015***
: My job started as a 6-month internship and eventually i started working there full time. I was part of the IT team and my main responsibilities were supporting users, troubleshooting and fixing sw & hw issues, administrating Active Directory, printing servers as well as the Avaya call center. Also i was developing MS SQL scripts-queries and crystal reports for the local ERP system. The biggest project that I was involved in at the time was the migration of the whole IT infrastructure(hw & sw) to newer devices and releases.

---

### Education {#education}

[University of West Attica](http://www.ice.uniwa.gr/)
: *MSc. - Cybersecurity*
  __2024 - now__
: Currently I am pursuing a master's degree in Cybersecurity.

[Technological Educational Institute of Athens](http://www.ice.uniwa.gr/)
: *BSc. - Computer Engineering*
  __2009 - 2015__
: With more than 40 courses with laboratories going from the basic circuits to complex projects. My studies hadn't been focused on one thing, but were directed to the whole spectrum of Computer Science.

---

### Certifications {#certifications}

[CKAD: Certified Kubernetes Application Developer](https://www.credly.com/badges/8b66b2a9-fc6a-4b07-bb51-b56bd40ac2d8)
: *The Linux Foundation*
  __Dec 2023__

[Microsoft Certified: Azure Network Engineer Associate (AZ-700)](https://www.credly.com/badges/ef20b6d5-f881-49c1-be82-722f0bb3c9ed/public_url)
: *Microsoft*
  __Apr 2022__

[Citrix Certified Professional - App Delivery and Security (CCP-AppDS)](https://www.credly.com/badges/9f7c3f40-9e86-4e7b-9a77-7199ea45a009/public_url)
: *Citrix Systems*
  __Dec 2021__

[Fortinet Network Security Expert Level 4: Certified Professional (NSE-4)](https://training.fortinet.com/badges/badge.php?hash=985e800f2ee0b0ab44d05b5a4fb999051c972208)
: *Fortinet*
  __Jul 2021__

[Citrix Certified Associate - App Delivery and Security (CCA-AppDS)](https://www.credly.com/badges/c14e57e2-a9ce-44b9-8921-8dad7f591ad9/public_url)
: *Citrix Systems*
  __Mar 2021__

[Check Point Certified Security Expert (CCSE) R80](https://www.youracclaim.com/badges/737f4da7-8241-4732-8274-bd8c8e7b2910/public_url)
: *Check Point Software Technologies*
  __May 2019__

[Check Point Certified Security Administrator (CCSA) R80](https://www.youracclaim.com/badges/10a28cb7-8435-4247-a16d-59a4ffc142ee/linked_in_profile)
: *Check Point Software Technologies*
  __Dec 2018__

[LFS101x.2: Introduction to Linux](https://verify.edx.org/cert/c955edd337cd444cb653c720bd44a56d)
: *LinuxFoundationX on edX <small>(c955edd337cd444cb653c720bd44a56d)</small>*
  __Oct 2015__

[Build a Backend REST API with Python & Django - Advanced](https://www.udemy.com/certificate/UC-97e3765f-36ca-43ba-b059-8692de6e89ba/)
: *Udemy<small>(UC-97e3765f-36ca-43ba-b059-8692de6e89ba)</small>*
  __Mar 2023__

[TOTAL: CompTIA Security+ Certification (SY0-501)](https://www.udemy.com/certificate/UC-0bb1544c-3aa1-4fba-8488-c643a9a17938/)
: *Udemy<small>(UC-0bb1544c-3aa1-4fba-8488-c643a9a17938)</small>*
  __Apr 2020__

[First Certificate in English](#certification)
: *University of Cambridge*

### Conference Presentations {#conferences}

[Network Failures: an Automated Incident Response approach](https://diavlos.grnet.gr/room/3152?eventid=15000&vod=12590_session)
: *GRNOG 15*
  __25 Oct 2023__

---

### Footer {#footer}

Gerasimos Tzakis -- [gertzakis@gmail.com](gertzakis@gmail.com)

---
